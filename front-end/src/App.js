import "./App.css";
import "./index.css";
import MyForm from "../src/components/Form";

const divStyle = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  height: "100vh",
  backgroundColor: "#f0e5d8",
};

const mainDivStyle = {
  backgroundColor: "#f2f2f2",
  padding: "20px",
  borderRadius: "5px",
  boxShadow: "0px 6px 49px -18px rgba(105,105,105,1)",
};

function App() {
  return (
    <div style={{ ...divStyle }}>
      <div style={{ ...mainDivStyle }}>
        <MyForm />
      </div>
    </div>
  );
}

export default App;
