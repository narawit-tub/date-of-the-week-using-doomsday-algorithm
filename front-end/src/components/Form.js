import { Form, Input, Button, Col, Row, Typography } from "antd";
import axios from "axios";
import { useState } from "react";

const MY_ENDPOINT = "http://localhost:7080/getday";

const divStyle = {
  backgroundColor: "#ffffff",
  padding: "20px",
  borderRadius: "5px",
};
const { Text } = Typography;

const MyForm = () => {
  const [dateState, setDateState] = useState();
  const [errorState, setErrorState] = useState();

  const onFinish = async ({ date }) => {
    // validation
    if (date.search(/\d{2}\/\d{2}\/\d{4}/i) !== 0 || date.length > 10) {
      return setErrorState("Please enter in the correct date, dd/mm/yyyy");
    }

    // sending to backend API
    const json = JSON.stringify({ date });
    const result = await axios.post(MY_ENDPOINT, json, {
      headers: {
        // Overwrite Axios's automatically set Content-Type
        "Content-Type": "application/json",
      },
    });

    setDateState(result.data.result);
  };

  const onInputformChange = () => {
    setDateState();
    setErrorState();
  };

  return (
    <div style={{ ...divStyle, width: "500px" }}>
      <Form name="basic" onFinish={onFinish} onChange={onInputformChange}>
        <Row align="top" justify="center">
          <Col style={{ marginRight: "20px" }} span={12}>
            <Form.Item label="Date" name="date">
              <Input placeholder="dd/mm/yyyy" />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Button type="dash" htmlType="submit">
              Get result
            </Button>
          </Col>
        </Row>
        {dateState && (
          <Row justify="center">
            <Col>
              <Text type="success" style={{ fontSize: "30px" }}>
                {dateState}
              </Text>
            </Col>
          </Row>
        )}
        {errorState && (
          <Row justify="center">
            <Col>
              <Text type="danger" style={{ fontSize: "15px" }}>
                {errorState}
              </Text>
            </Col>
          </Row>
        )}
      </Form>
    </div>
  );
};

export default MyForm;
