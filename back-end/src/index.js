const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const { dateInStringFormat } = require("./getDateOfTheDay");

const PORT = 7080;
const app = express();
app.use(bodyParser.json());
app.use(cors());

app.post("/getday/", (req, res) => {
  const date = req.body.date;
  const result = dateInStringFormat(date);

  console.log(req.body);
  res.send({ result });
});

app.listen(PORT, () => {
  console.log(`Server is up on port ${PORT}`);
});
