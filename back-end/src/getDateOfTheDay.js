function dateInStringFormat(dateStr) {
  const datePair = {
    0: "วันอาทิตย์",
    1: "วันจันทร์",
    2: "วันอังคาร",
    3: "วันพุธ",
    4: "วันพฤหัสบดี",
    5: "วันศุกร์",
    6: "วันเสาร์",
  };

  const monthPair = {
    0: "มกราคม",
    1: "กุมภาพันธ์",
    2: "มีนาคม",
    3: "เมษายน",
    4: "พฤษภาคม",
    5: "มิถุนายน",
    6: "กรกฏาคม",
    7: "สิงหาคม",
    8: "กันยายน",
    9: "ตุลาคม",
    10: "พฤศจิกายน",
    11: "ธันวาคม",
  };

  const dateIndex = getDateOfTheDay(dateStr);
  const day = dateStr.startsWith("0")
    ? dateStr.substr(1, 1)
    : dateStr.substr(0, 2);
  const month = parseInt(dateStr.substr(3, 2));
  const year = parseInt(dateStr.substr(-4));
  const resultStr = `${datePair[dateIndex]}ที่ ${day} ${monthPair[month - 1]} ${
    year + 543
  }`;
  return resultStr;
}

function getDateOfTheDay(dateStr) {
  const doomdayListLeapYear = [4, 1, 7, 4, 2, 6, 4, 1, 5, 3, 7, 5];
  const doomdayListNotLeapYear = [3, 7, 7, 4, 2, 6, 4, 1, 5, 3, 7, 5];

  let year = dateStr.substr(-4);
  let day = dateStr.startsWith("0")
    ? dateStr.substr(1, 1)
    : dateStr.substr(0, 2);

  day = parseInt(day);
  let month = parseInt(dateStr.substr(3, 2));
  let dayIndex;

  // Step Calculation
  let step1result = dateStr.substr(-2);

  let step2result = Math.floor(step1result / 12);
  let step3result = step1result - step2result * 12;
  let step4result = Math.floor(step3result / 4);

  //   leap year or not? because we use difference anchor day to calculate
  //  if year start with 1 (ex. 1997), anchor day is 3
  //  if year start with 2 (ex. 2006), anchor day is 2
  let step5result = dateStr.charAt(5) == 1 ? 3 : 2;
  let step6result = step2result + step3result + step4result + step5result;

  let step7result = step6result % 7;
  let leapYear = year % 4 == 0 ? true : false;
  if (year % 100 == 0 && year % 400 != 0) leapYear = false;

  let doomdayDay = leapYear
    ? doomdayListLeapYear[month - 1]
    : doomdayListNotLeapYear[month - 1];

  if (day < doomdayDay) {
    dayIndex = step7result - (doomdayDay - day);
  } else if (day > doomdayDay) {
    dayIndex = step7result + (day + doomdayDay);

    if (dayIndex >= 7) {
      dayIndex = dayIndex % 7;
    }
  } else {
    dayIndex = doomdayDay;
  }

  if (dayIndex < 0) dayIndex = 7 + dayIndex;
  return dayIndex;
}

module.exports.dateInStringFormat = dateInStringFormat;
